from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    tipo_usuario = models.ForeignKey(
        'TipoUsuario', db_column='id_tipo_usuario', on_delete=models.PROTECT, blank=True, null=True)

    def esAdministrador(self):
        return self.tipo_usuario.id_tipo_usuario == 1


class TipoUsuario(models.Model):

    id_tipo_usuario = models.AutoField(primary_key=True)
    tipo_usuario = models.CharField(max_length=20)

    class Meta:
        db_table = 'tipo_usuario'
